
import java.util.Scanner;

public class OX {
	static char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static int row, col;
	static char player = 'X';
	static int turn = 0;

	public static void main(String[] args) {
		showWelcome();
		while (true) {
			showTable();
			showTurn();
			input();

			if (checkWin()) {
				showTable();
				break;
			}
			switchPlayer();
		}
		showWin();
		showBye();
	}

	private static void showWelcome() {
		System.out.println("Welcome to XO Game");
	}

	private static void showTable() {
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private static void showTurn() {
		System.out.println(player + " turn ");
	}

	private static void input() {
		Scanner kb = new Scanner(System.in);
		System.out.print("Input row , col: ");
		row = kb.nextInt();
		col = kb.nextInt();
		if ((row > 3 || row < 1) || (col > 3 || col < 1)) {
			System.out.println("Sorry!!  wrong input  please input row , col again ");
			input();
		} else if (table[row - 1][col - 1] != '-') {
			System.out.println("Sorry!!  same input  please input row , col again ");
			input();
		}
		setTable();
	}

	private static void setTable() {
		table[row - 1][col - 1] = player;
	}

	private static boolean checkWin() {
		if (checkRow()) {
			return true;
		}
		if (checkCol()) {
			return true;
		}
		if (checkX()) {
			return true;
		}
		if (isDraw()) {
			return true;
		}
		return false;
	}

	private static boolean isDraw() {
		if (turn == 8)
			return true;
		return false;
	}

	private static boolean checkX() {
		if (checkX1()) {
			return true;
		}
		if (checkX2()) {
			return true;
		}
		return false;
	}

	private static boolean checkX1() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][i] != player)
				return false;
		}
		return true;
	}

	private static boolean checkX2() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][2 - i] != player)
				return false;
		}
		return true;
	}

	private static boolean checkRow(int rowInd) {
		for (int colInd = 0; colInd < table[rowInd].length; colInd++) {
			if (table[rowInd][colInd] != player)
				return false;
		}
		return true;
	}

	private static boolean checkRow() {
		for (int rowInd = 0; rowInd < table.length; rowInd++) {
			if (checkRow(rowInd))
				return true;
		}
		return false;
	}

	private static boolean checkCol(int colInd) {
		for (int rowInd = 0; rowInd < table[colInd].length; rowInd++) {
			if (table[rowInd][colInd] != player)
				return false;
		}
		return true;
	}

	private static boolean checkCol() {
		for (int colInd = 0; colInd < table[0].length; colInd++) {
			if (checkCol(colInd))
				return true;
		}
		return false;
	}

	private static void switchPlayer() {
		if (player == 'X') {
			player = 'O';
		} else if (player == 'O') {
			player = 'X';
		}
		turn++;
	}

	private static void showWin() {
		if (isDraw()) {
			System.out.println("Draw");
		} else {
			System.out.println(player + " Win...");
		}
	}

	private static void showBye() {
		System.out.println("Bye Bye...");

	}
}